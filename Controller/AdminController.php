<?php

namespace Incolab\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Incolab\ForumBundle\Entity\Category;

use Incolab\ForumBundle\Form\Type\CategoryType;

class AdminController extends Controller
{
    
    public function indexAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            
            return $this->forward('IncolabForumBundle:Admin:parentCategoryAdd', array('request' => $request));        
        }
            
        $categories = $this->getDoctrine()->getRepository('IncolabForumBundle:Category')->getParentsWithChilds();  
        
        $form = $this->createForm(CategoryType::class, new Category());
        
        return $this->render('IncolabForumBundle:Admin:index.html.twig', array('categories' => $categories, 'parentCatForm' => $form->createView()));
    
    }

    public function parentCategoryAddAction(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository('IncolabForumBundle:Category')->getParents();
        
        $insertParentCat = new Category();
        $form = $this->createForm(CategoryType::class, $insertParentCat);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->container->get('incolab_forum.category_manager')->addParent($categories, $insertParentCat);
            
            $this->addFlash('notice', 'Category was added');
            
            return $this->redirectToRoute('incolab_forum_admin_homepage');
        }
        
        return $this->render('IncolabForumBundle:Admin:parentCategoryAdd.html.twig', array('parentCatForm' => $form->createView()));
    }
    
    public function childCategoryAddAction($slugParent, Request $request)
    {
        $parentCat = $this->getDoctrine()->getRepository('IncolabForumBundle:Category')->getParentBySlug($slugParent);
        
        if ($parentCat === NULL) {
            throw $this->createNotFoundException('Aucune catégorie à cette adresse');
        }
        if ($request->isMethod('POST')) {
            $category = new Category();
            $form = $this->createForm(CategoryType::class, $category);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $category->setParent($parentCat);
                $this->container->get('incolab_forum.category_manager')->addChild($category);
                
                return $this->redirectToRoute('incolab_forum_admin_child_category_add', array('slugParent' => $parentCat->getSlug()));
            }
            
            return $this->render('IncolabForumBundle:Admin:childCategoryAdd.html.twig', array('category' => $parentCat, 'childCatForm' => $form->createView()));
        }
        
        $form = $this->createForm(CategoryType::class, new Category());
        return $this->render('IncolabForumBundle:Admin:childCategoryAdd.html.twig', array('category' => $parentCat, 'childCatForm' => $form->createView()));
    }

}
