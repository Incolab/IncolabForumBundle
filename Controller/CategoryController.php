<?php

namespace Incolab\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Incolab\ForumBundle\Entity\Topic;
use Incolab\ForumBundle\Entity\Post;
use Incolab\ForumBundle\Form\Type\TopicType;
use Incolab\ForumBundle\Form\Type\PostType;

class CategoryController extends Controller
{
    public function parentCategoryAction($slugParentCat)
    {
        $parentCategory = $this->getDoctrine()->getrepository('IncolabForumBundle:Category')
                ->getParentCategoryBySlug($slugParentCat);
        
        if ($parentCategory === NULL) {
            throw $this->createNotFoundException('Aucune Catégorie pour cette adresse.');
        } else {
            return $this->render('IncolabForumBundle:Category:parentCategory.html.twig', array('parentCategory' => $parentCategory));
        }
    }
    
    public function categoryAction($slugParentCat, $slugCat)
    {   
        $category = $this->getDoctrine()->getrepository('IncolabForumBundle:Category')->getCategoryBySlugAndParentSlug($slugCat, $slugParentCat);
        
        if ($category === NULL) {
            throw $this->createNotFoundException('Aucune Catégorie pour cette adresse.');
        }

        $form = $this->createForm(TopicType::class, new Topic(), array(
                                                                       'action' => $this->generateUrl('incolab_forum_category_topic_create',
                                                                                                      array('slugParentCat' => $category->getParent()->getSlug(),
                                                                                                            'slugCat' => $category->getSlug())
                                                                                                      ),
                                                                       'method' => 'POST'
                                                                       )
                                  );   
            
        return $this->render('IncolabForumBundle:Category:category.html.twig', array('category' => $category,
                                                                                     'topics' => $category->getTopics(),
                                                                                     'topicForm' => $form->createView())
                             );
    }
    
    public function topicShowAction($slugParentCat, $slugCat, $slugTopic)
    {
        $topic = $this->getDoctrine()->getRepository('IncolabForumBundle:Topic')->getTopicBySlugTopicCatParentCat($slugTopic, $slugCat, $slugParentCat);
        if ($topic === NULL) {
            throw $this->createNotFoundException('This topic don\'t exists');
        }
        
        $form = $this->createForm(PostType::class, new Post(), array('action' => $this->generateUrl('incolab_forum_post_create',
                                                                                                    array('slugParentCat' => $topic->getCategory()->getParent()->getSlug(),
                                                                                                          'slugCat' => $topic->getCategory()->getSlug(),
                                                                                                          'slugTopic' => $topic->getSlug())
                                                                                                    ),
                                                                     'method' => 'POST'
                                                                     )
                                  );
        
        
        return $this->render('IncolabForumBundle:Topic:show.html.twig', array('topic' => $topic, 'postForm' => $form->createView()));
    }
    
    public function topicNewAction($slugParentCat, $slugCat)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        
        $repository = $this->getDoctrine()->getRepository('IncolabForumBundle:Category');
        
        $parentCat = $repository->createQueryBuilder('p')
            ->where('p.slug = :parentSlug AND p.parent IS NULL')
            ->addSelect('childs')
            ->andWhere('childs.slug = :childSlug')
            ->leftJoin('p.childs', 'childs')
            ->setParameters(array(':parentSlug' => $slugParentCat, ':childSlug' => $slugCat))
            ->getQuery()->getOneOrNullResult();
        
        // le form sera la [GET]
        if ($parentCat === NULL) {
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }
        $form = $this->createForm(TopicType::class, new Topic());
        
        return $this->render('IncolabForumBundle:Topic:add.html.twig', array('childCatForm' => $form->createView(), 'parentCat' => $parentCat));
    }
    
    public function topicCreateAction($slugParentCat, $slugCat, Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        
        $parentCat = $this->getDoctrine()->getRepository('IncolabForumBundle:Category')->getCategoryForInsertTopic($slugCat, $slugParentCat);
        
        // traitement du [POST]
        if ($parentCat === NULL) {
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        } else {
            $topic = new Topic();
            $form = $this->createForm(TopicType::class, $topic);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $topic->setAuthor($this->getUser());
                $category = $parentCat->getChildBySlug($slugCat);
                $topic->setCategory($category);
                $this->container->get('incolab_forum.topic_manager')->add($topic);
                
                return $this->redirectToRoute('incolab_forum_topic_show',
                                              array('slugParentCat' => $topic->getCategory()->getParent()->getSlug(),
                                                  'slugCat' => $topic->getCategory()->getSlug(),
                                                  'slugTopic' => $topic->getSlug())
                                              );
                
            } else {
                return $this->render('IncolabForumBundle:Topic:add.html.twig', array('childCatForm' => $form->createView(),
                                                                                     'parentCat' => $parentCat));
            }
        }
    }
    
    public function postAddAction($slugParentCat, $slugCat, $slugTopic, Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        
        $topic = $this->getDoctrine()->getRepository('IncolabForumBundle:Topic')
                ->getTopicForInsertPost($slugTopic, $slugCat, $slugParentCat);
        
        if ($topic === NULL) {
            throw $this->createNotFoundException('This topic don\'t exists');
        }
        
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $post->setTopic($topic);
            $post->setAuthor($this->getUser());
            $this->container->get('incolab_forum.post_manager')->add($post);
            
            $this->addFlash('success','Post added with success');
            
            return $this->redirectToRoute('incolab_forum_topic_show', array('slugParentCat' => $topic->getCategory()->getParent()->getSlug(),
                                                                            'slugCat' => $topic->getCategory()->getSlug(),
                                                                            'slugTopic' => $topic->getSlug()
                                                                            )
                                          );
        }
        
        return $this->render('IncolabForumBundle:Topic:show.html.twig', array('topic' => $topic, 'postForm' => $form->createView()));
    }
    
}
