<?php

namespace Incolab\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $categories = $this->getDoctrine()->getRepository('IncolabForumBundle:Category')->getIndex();
            return $this->render('IncolabForumBundle:Default:index.html.twig', array('categories' => $categories));
        }
        
        $user = $this->getUser();
        $categories = $this->getDoctrine()->getRepository('IncolabForumBundle:Category')->getIndex($user->getForumRoles());
        return $this->render('IncolabForumBundle:Default:index.html.twig', array('categories' => $categories));
    }
}
