<?php

namespace Incolab\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="forum_category")
 * @ORM\Entity(repositoryClass="Incolab\ForumBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Incolab\ForumBundle\Entity\Category", cascade={"persist"}, inversedBy="childs")
     */
    private $parent;
    
    /**
     * @var string
     *
     * @ORM\Column(name="read_roles", type="array", options={"default"="a:0:{}"})
     */
    private $readRoles;
    
    /**
     * @var string
     *
     * @ORM\Column(name="write_roles", type="array", options={"default"="a:0:{}"})
     */
    private $writeRoles;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="Incolab\ForumBundle\Entity\Category", mappedBy="parent")
     */
    private $childs;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;
    
    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="Incolab\ForumBundle\Entity\Topic", mappedBy="category")
     */
    private $topics;

    /**
     * @var int
     *
     * @ORM\Column(name="num_topics", type="integer")
     */
    private $numTopics;

    /**
     * @var int
     *
     * @ORM\Column(name="num_posts", type="integer")
     */
    private $numPosts;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Incolab\ForumBundle\Entity\Topic", cascade="persist")
     */
    private $lastTopic;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Incolab\ForumBundle\Entity\Post", cascade="persist")
     */
    private $lastPost;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->childs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->topics = new \Doctrine\Common\Collections\ArrayCollection();
        $this->numPosts = 0;
        $this->numTopics = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Category
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    /**
     * Increments the category position
     */
    public function incrementPosition()
    {
        $this->position++;
        
        return $this;
    }
    
    /**
     * Decrements the category position
     */
    public function decrementPosition()
    {
        $this->position--;
        
        return $this;
    }

    /**
     * Set numTopics
     *
     * @param integer $numTopics
     *
     * @return Category
     */
    public function setNumTopics($numTopics)
    {
        $this->numTopics = $numTopics;

        return $this;
    }

    /**
     * Get numTopics
     *
     * @return integer
     */
    public function getNumTopics()
    {
        return $this->numTopics;
    }
    
    /**
     * Increments the number of topics
     */
    public function incrementNumTopics()
    {
        $this->numTopics++;
        
        return $this;
    }
    
    /**
     * Decrements the number of topics
     */
    public function decrementNumTopics()
    {
        $this->numTopics--;
        
        return $this;
    }

    /**
     * Set numPosts
     *
     * @param integer $numPosts
     *
     * @return Category
     */
    public function setNumPosts($numPosts)
    {
        $this->numPosts = $numPosts;

        return $this;
    }

    /**
     * Get numPosts
     *
     * @return integer
     */
    public function getNumPosts()
    {
        return $this->numPosts;
    }
    
    /**
     * Increments the number of posts
     */
    public function incrementNumPosts()
    {
        $this->numPosts++;
        
        return $this;
    }
    
    /**
     * Decrements the number of posts
     */
    public function decrementNumPosts()
    {
        $this->numPosts--;
        
        return $this;
    }

    /**
     * Set parent
     *
     * @param \Incolab\ForumBundle\Entity\Category $parent
     *
     * @return Category
     */
    public function setParent(\Incolab\ForumBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Incolab\ForumBundle\Entity\Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param \Incolab\ForumBundle\Entity\Category $child
     *
     * @return Category
     */
    public function addChild(\Incolab\ForumBundle\Entity\Category $child)
    {
        $this->childs[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Incolab\ForumBundle\Entity\Category $child
     */
    public function removeChild(\Incolab\ForumBundle\Entity\Category $child)
    {
        $this->childs->removeElement($child);
    }

    /**
     * Get childs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChilds()
    {
        return $this->childs;
    }
    
    /**
     * Get child by slug
     *
     * @return Category
     */
    public function getChildBySlug($slug)
    {
        foreach ($this->childs as $child) {
            if ($child->getSlug() === $slug){
                return $child;
            }
        }
    }

    /**
     * Add topic
     *
     * @param \Incolab\ForumBundle\Entity\Topic $topic
     *
     * @return Category
     */
    public function addTopic(\Incolab\ForumBundle\Entity\Topic $topic)
    {
        $this->topics[] = $topic;

        return $this;
    }

    /**
     * Remove topic
     *
     * @param \Incolab\ForumBundle\Entity\Topic $topic
     */
    public function removeTopic(\Incolab\ForumBundle\Entity\Topic $topic)
    {
        $this->topics->removeElement($topic);
    }

    /**
     * Get topics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * Set lastTopic
     *
     * @param \Incolab\ForumBundle\Entity\Topic $lastTopic
     *
     * @return Category
     */
    public function setLastTopic(\Incolab\ForumBundle\Entity\Topic $lastTopic = null)
    {
        $this->lastTopic = $lastTopic;

        return $this;
    }

    /**
     * Get lastTopic
     *
     * @return \Incolab\ForumBundle\Entity\Topic
     */
    public function getLastTopic()
    {
        return $this->lastTopic;
    }

    /**
     * Set lastPost
     *
     * @param \Incolab\ForumBundle\Entity\Post $lastPost
     *
     * @return Category
     */
    public function setLastPost(\Incolab\ForumBundle\Entity\Post $lastPost = null)
    {
        $this->lastPost = $lastPost;

        return $this;
    }

    /**
     * Get lastPost
     *
     * @return \Incolab\ForumBundle\Entity\Post
     */
    public function getLastPost()
    {
        return $this->lastPost;
    }
    
    /**
     * Add readRoles
     *
     * @param array $readRole
     *
     * @return Category
     */
    public function addReadRole($readRole)
    {
        $readRole = strtoupper($readRole);
        if (!in_array($readRole, $this->readRoles, true)) {
            $this->readRoles[] = $readRole;
        }

        return $this;
    }

    /**
     * Set readRoles
     *
     * @param array $readRoles
     *
     * @return Category
     */
    public function setReadRoles(array $readRoles)
    {
        $this->readRoles = array();
        foreach ($readRoles as $role) {
            $this->addReadRole($role);
        }

        return $this;
    }

    /**
     * Get readRoles
     *
     * @return array
     */
    public function getReadRoles()
    {
        return $this->readRoles;
    }
    
    /*
     * @param string $readRole
     *
     * @return boolean
     */
    public function hasReadRole($readRole)
    {
        return in_array(strtoupper($readRole), $this->getReadRoles(), true);
    }
    
    /**
     * Add writeRoles
     *
     * @param array $writeRole
     *
     * @return Category
     */
    public function addWriteRole($writeRole)
    {
        $writeRole = strtoupper($writeRole);
        if (!in_array($writeRole, $this->writeRoles, true)) {
            $this->writeRoles[] = $writeRole;
        }

        return $this;
    }

    /**
     * Set writeRoles
     *
     * @param array $writeRoles
     *
     * @return Category
     */
    public function setWriteRoles($writeRoles)
    {
        $this->writeRoles = array();
        foreach ($writeRoles as $role) {
            $this->addWriteRole($role);
        }

        return $this;
    }

    /**
     * Get writeRoles
     *
     * @return array
     */
    public function getWriteRoles()
    {
        return $this->writeRoles;
    }
    
    /*
     * @param string $writeRole
     *
     * @return boolean
     */
    public function hasWriteRole($writeRole)
    {
        return in_array(strtoupper($writeRole), $this->getWriteRoles(), true);
    }
}
