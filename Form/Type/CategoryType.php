<?php

namespace Incolab\ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            //->add('slug')
            ->add('readRoles', CollectionType::class, array('entry_type'   => ChoiceType::class,
                                                            'allow_add' => true,
                                                            'entry_options'  => array('choices'  => array('Admin' => 'ROLE_ADMIN',
                                                                                                          'User'     => 'ROLE_USER',
                                                                                                          'Developper'    => 'ROLE_DEVELOPPER',
                                                                                                          'Public' => 'ROLE_PUBLIC'
                                                                                                          ),
                                                                                      'required'  => true,
                                                                                      'attr'      => array('class' => 'read-roles-box'))
                                                            )
                  )
            ->add('writeRoles', CollectionType::class, array('entry_type'   => ChoiceType::class,
                                                             'allow_add' => true,
                                                             'entry_options'  => array('choices'  => array('Admin' => 'ROLE_ADMIN',
                                                                                                           'User'     => 'ROLE_USER',
                                                                                                           'Developper'    => 'ROLE_DEVELOPPER',
                                                                                                           'Public' => 'ROLE_PUBLIC'
                                                                                                           ),
                                                                                       'required'  => true,
                                                                                       'attr'      => array('class' => 'write-roles-box'))
                                                            )
                  )
            /*->add('readRoles', ChoiceType::class, array('choices'  => array('Admin' => 'ROLE_ADMIN',
                                                                            'User'     => 'ROLE_USER',
                                                                            'Developper'    => 'ROLE_DEVELOPPER',
                                                                            'Public' => 'ROLE_PUBLIC'
                                                                            )
                                                        ))
            
            ->add('writeRoles', ChoiceType::class, array('choices'  => array('Admin' => 'ROLE_ADMIN',
                                                                            'User'     => 'ROLE_USER',
                                                                            'Developper'    => 'ROLE_DEVELOPPER',
                                                                            )
            
                                                        ))
            */
            ->add('position', IntegerType::class)
            /*
            ->add('numTopics')
            ->add('numPosts')
            ->add('lastPost')
            ->add('lastTopic')
            */
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incolab\ForumBundle\Entity\Category'
        ));
    }
}
