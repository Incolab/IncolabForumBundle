<?php

namespace Incolab\ForumBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends \Doctrine\ORM\EntityRepository
{
    private $public_role = array('ROLE_PUBLIC');
    
    public function getIndex($roles = array())
    {
        $categories = $this->createQueryBuilder('p')
            ->leftJoin('p.childs', 'c')
            ->leftJoin('c.lastTopic', 't')
            ->leftJoin('c.lastPost', 'l')
            ->leftJoin('l.topic', 'lt')
            ->leftJoin('t.author', 'a')
            ->leftJoin('l.author', 'la')
            ->addSelect('c')
            ->addSelect('partial t.{id, author, subject, category, slug, createdAt}')
            ->addSelect('partial l.{id, topic, author, createdAt}')
            ->addSelect('partial a.{id, username}')
            ->addSelect('partial la.{id, username}')
            ->addSelect('partial lt.{id, author, subject, slug, createdAt}')
            ->where('p.parent IS NULL')
            ->orderBy('p.position', 'ASC')
            ->addOrderBy('c.position', 'ASC')
            ->getQuery()->getResult();
        
        if ($roles) {
            $filteredCats = $this->filterCategories($categories, $roles);
            return $filteredCats;
        }
        
        $filteredCats = $this->filterCategories($categories, $this->public_role);
        return $filteredCats;
    }
    
    public function getParents()
    {
        $categories = $this->createQueryBuilder('p')
            ->where('p.parent IS NULL')
            ->orderBy('p.position', 'ASC')
            ->getQuery()->getResult();
        
        return $categories;
    }
    
    public function getParentsWithChilds()
    {
        $categories = $this->createQueryBuilder('p')
            ->where('p.parent IS NULL')
            ->orderBy('p.position', 'ASC')
            ->addSelect('childs')
            ->leftJoin('p.childs', 'childs')
            ->addOrderBy('childs.position', 'ASC')
            ->getQuery()->getResult();
        
        return $categories;
    }
    
    public function getParentBySlug($slug)
    {
        $parentCat = $this->createQueryBuilder('c')
                ->where('c.slug = :slug AND c.parent IS NULL')
                ->setParameter(':slug', $slug)
                ->getQuery()->setMaxResults(1)->getOneOrNullResult();
        return $parentCat;
    }
    
    // A changer pour le paginator
    public function getParentCategoryPageBySlug($slug, $page = 1, $maxperpage = 10)
    {
        $parentCategory = $this->createQueryBuilder('p')
            ->leftJoin('p.childs', 'c')
            ->leftJoin('c.lastTopic', 'l')
            ->leftJoin('c.lastPost', 'm')
            ->leftJoin('l.author', 'a')
            ->leftJoin('m.author', 'b')
            ->addSelect('c')
            ->addSelect('partial l.{id, author, subject, category, slug, createdAt}')
            ->addSelect('partial m.{id, topic, author, createdAt}')
            ->addSelect('partial a.{id, username}')
            ->addSelect('partial b.{id, username}')
            ->where('p.slug = :slug AND p.parent IS NULL')
            ->setParameter(':slug', $slug)
            //->getQuery()->getOneOrNullResult()
            ->setFirstResult(($page -1) * $maxperpage)
            ->setMaxResults($maxperpage)
            ;
            
        return new Paginator($parentCategory);
    }
    
    public function getParentCategoryBySlug($slug)
    {
        $parentCategory = $this->createQueryBuilder('p')
            ->leftJoin('p.childs', 'c')
            ->leftJoin('c.lastTopic', 'l')
            ->leftJoin('c.lastPost', 'm')
            ->leftJoin('l.author', 'a')
            ->leftJoin('m.author', 'b')
            ->addSelect('c')
            ->addSelect('partial l.{id, author, subject, category, slug, createdAt}')
            ->addSelect('partial m.{id, topic, author, createdAt}')
            ->addSelect('partial a.{id, username}')
            ->addSelect('partial b.{id, username}')
            ->where('p.slug = :slug AND p.parent IS NULL')
            ->setParameter(':slug', $slug)
            ->getQuery()->getOneOrNullResult();
            
        return $parentCategory;
    }
    
    public function getCategoryBySlugAndParentSlug($slug, $parentSlug)
    {
        $category = $this->createQueryBuilder('c')
                ->leftJoin('c.parent', 'p')
                ->leftJoin('c.topics', 't')
                ->leftJoin('t.author', 'ta')
                ->leftJoin('t.lastPost', 'l')
                ->leftJoin('l.author', 'la')
                ->addSelect('p')
                ->addSelect('t')
                ->addSelect('partial ta.{id, username}')
                ->addSelect('l')
                ->addSelect('partial la.{id, username}')
                ->where('c.slug = :slugCat AND p.slug = :slugParent')
                ->orderBy('l.createdAt', 'DESC')
                ->setParameters(array(':slugCat' => $slug, ':slugParent' => $parentSlug ))
                ->getQuery()->getOneOrNullResult();
        return $category;
    }
    
    // A changer pour le paginator
    public function getCategoryPageBySlugAndParentSlug($slug, $parentSlug)
    {
        $category = $this->createQueryBuilder('c')
                ->leftJoin('c.parent', 'p')
                ->leftJoin('c.topics', 't')
                ->leftJoin('t.author', 'ta')
                ->leftJoin('t.lastPost', 'l')
                ->leftJoin('l.author', 'la')
                ->addSelect('p')
                ->addSelect('distinct t')
                ->addSelect('partial ta.{id, username}')
                ->addSelect('l')
                ->addSelect('partial la.{id, username}')
                ->where('c.slug = :slugCat AND p.slug = :slugParent')
                ->setParameters(array(':slugCat' => $slug, ':slugParent' => $parentSlug ))
                ->getQuery()->getOneOrNullResult();
        return $category;
    }
    
    public function getCategoryForInsertTopic($slug, $parentSlug)
    {
        $category = $this->createQueryBuilder('p')
                ->where('p.slug = :parentSlug AND p.parent IS NULL')
                ->addSelect('childs')
                ->andWhere('childs.slug = :childSlug')
                ->leftJoin('p.childs', 'childs')
                ->setParameters(array(':parentSlug' => $parentSlug, ':childSlug' => $slug))
                ->getQuery()->getOneOrNullResult();
        return $category;
    }
    
    private function filterCategories($categories = array(), $roles = array())
    {
        $filteredCat = array();
        
        foreach($categories as $categorie) {
            $parentcat = NULL;
            
            foreach ($roles as $role) {
                if ($categorie->hasReadRole($role)) {
                    $parentcat = $categorie;
                    break;
                }
            }
            
            if ($parentcat) {
                $childs = $parentcat->getChilds();
                if ($childs) {
                    foreach ($childs as $child) {
                        foreach ($roles as $role) {
                            if (!$child->hasReadRole($role)) {
                                $parentcat->removeChild($child);
                                break;
                            }
                        }
                    }
                }
                $filteredCat[] = $parentcat;
            }
        }
        
        return $filteredCat;
    }
}
