<?php

namespace Incolab\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="forum_category")
 * @ORM\Entity(repositoryClass="Incolab\ForumBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var int
     *
     * @ORM\Column(name="num_topics", type="integer")
     */
    private $numTopics;

    /**
     * @var int
     *
     * @ORM\Column(name="num_posts", type="integer")
     */
    private $numPosts;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="last_topic", type="object", nullable=true)
     */
    private $lastTopic;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="last_post", type="object", nullable=true)
     */
    private $lastPost;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return Category
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Category
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set numTopics
     *
     * @param integer $numTopics
     *
     * @return Category
     */
    public function setNumTopics($numTopics)
    {
        $this->numTopics = $numTopics;

        return $this;
    }

    /**
     * Get numTopics
     *
     * @return int
     */
    public function getNumTopics()
    {
        return $this->numTopics;
    }

    /**
     * Set numPosts
     *
     * @param integer $numPosts
     *
     * @return Category
     */
    public function setNumPosts($numPosts)
    {
        $this->numPosts = $numPosts;

        return $this;
    }

    /**
     * Get numPosts
     *
     * @return int
     */
    public function getNumPosts()
    {
        return $this->numPosts;
    }

    /**
     * Set lastTopic
     *
     * @param \stdClass $lastTopic
     *
     * @return Category
     */
    public function setLastTopic($lastTopic)
    {
        $this->lastTopic = $lastTopic;

        return $this;
    }

    /**
     * Get lastTopic
     *
     * @return \stdClass
     */
    public function getLastTopic()
    {
        return $this->lastTopic;
    }

    /**
     * Set lastPost
     *
     * @param \stdClass $lastPost
     *
     * @return Category
     */
    public function setLastPost($lastPost)
    {
        $this->lastPost = $lastPost;

        return $this;
    }

    /**
     * Get lastPost
     *
     * @return \stdClass
     */
    public function getLastPost()
    {
        return $this->lastPost;
    }
}

